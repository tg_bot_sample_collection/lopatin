import os
import time
import qrcode
from aiogram import Bot, types, Dispatcher

# QR-code template
QR = qrcode.QRCode(
    version=None,
    error_correction=qrcode.constants.ERROR_CORRECT_H,
    box_size=10,
    border=4,
)

# Папка для сохранения QR-codes
TARGET_DIR = 'qrcodes' + os.sep + time.strftime('%Y%m%d%H%M%S') + '.png'

# Имя database
DB_FILE = 'db_users.db'

# Telegram bot
TOKEN = '?????????????????????????///'

bot = Bot(token=TOKEN, parse_mode=types.ParseMode.HTML)
dp = Dispatcher(bot)

#Handlers
import aiogram.utils.markdown as aimd
import logging
from app.answers import kb_start, help_mssg, info_mssg, error_mssg
from app.sq_statement import get_users, set_user

async def start_command(message: types.Message):
    user_id = message.from_user.id
    result = get_users(DB_FILE)
    for i in result:
        if user_id == i[0]:
            break
    else:
        first_name = message.from_user.first_name
        username = message.from_user.username
        logging.info(f"New user: {first_name}")
        set_user(DB_FILE, user_id, first_name, username)
    await message.reply(
        f"Привет, <b>{aimd.quote_html(message.from_user.first_name)}</b>! Я могу сгенерировать <u>QR-code</u>.\n"
        f"Отправь мне текст, (К примеру ссылку) И я сделаю для неё QR-code.\n"
        f"Отправь /info что бы увидеть информацию обо мне",
        reply_markup=kb_start)


async def help_command(message: types.Message):
    await message.reply(help_mssg)

async def info_command(message: types.Message):
    await message.answer(info_mssg)

async def qr_code_creator(message: types.Message):
    data = message.text
    logging.info(data)
    QR.add_data(data)
    QR.make(fit=True)
    img = QR.make_image(fill_color="black", back_color="white").convert('RGB')
    img.save(TARGET_DIR)
    with open(TARGET_DIR, 'rb') as qrcode:
        await bot.send_photo(message.from_user.id, qrcode)
    QR.clear()
    try:
        os.remove(TARGET_DIR)
    except FileNotFoundError:
        pass

async def other_command(message: types.Message):
    await message.reply(error_mssg)

def register_handlers_bot(dp: Dispatcher):
    dp.register_message_handler(start_command, commands=['start'])
    dp.register_message_handler(help_command, commands=['help'])
    dp.register_message_handler(info_command, commands=['info'])
    dp.register_message_handler(qr_code_creator, content_types=types.ContentType.TEXT)
    dp.register_message_handler(other_command, content_types=types.ContentType.PHOTO)
