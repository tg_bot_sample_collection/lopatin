from aiogram.types import KeyboardButton, ReplyKeyboardMarkup

help_mssg = "Привет, отправь мне сообщение, и я отправлю тебе QR cod ;)"
info_mssg = """
Создатель Лопатин Иван\n
Спасибо за использование бота.
"""
error_mssg = "Я не знаю как обработать этот запрос"

button_help = KeyboardButton('/help')
button_info = KeyboardButton('/info')

kb_start = ReplyKeyboardMarkup(
    resize_keyboard=True, one_time_keyboard=True
).add(button_help, button_info)
