import logging
from aiogram.utils import executor
from app.config import dp, register_handlers_bot




if __name__ == '__main__':
    logging.basicConfig(
        level=logging.INFO,
        format="[%(asctime)s](%(levelname)s): \"%(filename)s\", line:%(lineno)d, %(funcName)s - %(message)s",
        filename='botLogs.log')
    register_handlers_bot(dp)
    print('Бот онлайн')
    executor.start_polling(dp, skip_updates=True)
